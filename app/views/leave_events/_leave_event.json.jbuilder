json.extract! leave_event, :id, :start_time, :end_time, :description, :created_at, :updated_at
json.url leave_event_url(leave_event, format: :json)

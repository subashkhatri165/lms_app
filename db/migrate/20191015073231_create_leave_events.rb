class CreateLeaveEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :leave_events do |t|
      t.datetime :start_time
      t.datetime :end_time
      t.string :status, default: "Pending"       
      t.text :description

      t.timestamps
    end
  end
end

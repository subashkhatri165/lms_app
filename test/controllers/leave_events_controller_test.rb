require 'test_helper'

class LeaveEventsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @leave_event = leave_events(:one)
  end

  test "should get index" do
    get leave_events_url
    assert_response :success
  end

  test "should get new" do
    get new_leave_event_url
    assert_response :success
  end

  test "should create leave_event" do
    assert_difference('LeaveEvent.count') do
      post leave_events_url, params: { leave_event: { description: @leave_event.description, end_time: @leave_event.end_time, start_time: @leave_event.start_time } }
    end

    assert_redirected_to leave_event_url(LeaveEvent.last)
  end

  test "should show leave_event" do
    get leave_event_url(@leave_event)
    assert_response :success
  end

  test "should get edit" do
    get edit_leave_event_url(@leave_event)
    assert_response :success
  end

  test "should update leave_event" do
    patch leave_event_url(@leave_event), params: { leave_event: { description: @leave_event.description, end_time: @leave_event.end_time, start_time: @leave_event.start_time } }
    assert_redirected_to leave_event_url(@leave_event)
  end

  test "should destroy leave_event" do
    assert_difference('LeaveEvent.count', -1) do
      delete leave_event_url(@leave_event)
    end

    assert_redirected_to leave_events_url
  end
end

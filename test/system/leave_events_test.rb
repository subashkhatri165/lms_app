require "application_system_test_case"

class LeaveEventsTest < ApplicationSystemTestCase
  setup do
    @leave_event = leave_events(:one)
  end

  test "visiting the index" do
    visit leave_events_url
    assert_selector "h1", text: "Leave Events"
  end

  test "creating a Leave event" do
    visit leave_events_url
    click_on "New Leave Event"

    fill_in "Description", with: @leave_event.description
    fill_in "End time", with: @leave_event.end_time
    fill_in "Start time", with: @leave_event.start_time
    click_on "Create Leave event"

    assert_text "Leave event was successfully created"
    click_on "Back"
  end

  test "updating a Leave event" do
    visit leave_events_url
    click_on "Edit", match: :first

    fill_in "Description", with: @leave_event.description
    fill_in "End time", with: @leave_event.end_time
    fill_in "Start time", with: @leave_event.start_time
    click_on "Update Leave event"

    assert_text "Leave event was successfully updated"
    click_on "Back"
  end

  test "destroying a Leave event" do
    visit leave_events_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Leave event was successfully destroyed"
  end
end
